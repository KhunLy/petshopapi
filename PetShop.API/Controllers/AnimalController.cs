﻿using Microsoft.AspNetCore.Mvc;
using PetShop.API.Authorizations;
using PetShop.API.DTO;
using PetShop.API.Session;
using PetShop.API.UOW;
using System.Collections.Generic;
using System.Linq;

namespace PetShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalController : MasterController
    {
        public AnimalController([FromServices] UnitOfWork uow, [FromServices] SessionService session) : base(uow, session)
        {
        }

        [HttpPost]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = typeof(AnimalDTO))]
        public IActionResult Post([FromBody]AnimalDTO dto)
        {
            IAnimalRepository repo = _uow.Get<IAnimalRepository>();
            repo.Insert(dto);
            _uow.Save();
            return Ok(dto);
        }

        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<AnimalDTO>))]
        public IActionResult Get()
        {
            return Ok(_uow.Get<IAnimalRepository>().GetAll());
        }

        [HttpDelete]
        [Route("{name}")]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = null)]
        public IActionResult Delete(string name)
        {
            if (_uow.Get<IAnimalRepository>().Delete(name))
            {
                _uow.Save();
                return Ok();
            }
            return BadRequest();
        }
    }
}
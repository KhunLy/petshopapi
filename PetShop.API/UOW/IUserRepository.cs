﻿using System.Collections.Generic;
using PetShop.API.DTO;

namespace PetShop.API.UOW
{
    public interface IUserRepository
    {
        bool Delete(int key);
        UserDTO Get(int key);
        IEnumerable<UserDTO> GetAll();
        UserDTO GetByCredentials(LoginDTO login);
        UserDTO GetByEmail(string email);
        UserDTO Insert(RegisterDTO entity);
        bool Update(UserUpdateDTO entity);
    }
}
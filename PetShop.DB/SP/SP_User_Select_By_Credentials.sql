﻿CREATE PROCEDURE [dbo].[SP_User_Select_By_Credentials]
	@email NVARCHAR(255),
	@password NVARCHAR(255)
AS
	SELECT *
	FROM [User]
	WHERE Id = (
		SELECT Id
		FROM [User]
		WHERE Email = @email AND [Password] = dbo.UDF_Hash(@password, Salt)
	)
RETURN 0

﻿using Microsoft.AspNetCore.Mvc;
using PetShop.API.Authorizations;
using PetShop.API.DTO;
using PetShop.API.Session;
using PetShop.API.UOW;
using System.Collections.Generic;
using ToolBox.Mappers;

namespace PetShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PetController : MasterController
    {
        public PetController([FromServices] UnitOfWork uow, [FromServices] SessionService session) : base(uow, session)
        {
        }

        [HttpGet]
        [Route("{id}")]
        [Produces("application/json", Type = typeof(PetDetailsDTO))]
        public IActionResult Get(int id)
        {
            PetDetailsDTO toReturn = _uow.Get<IPetRepository>().Get(id);
            if (toReturn == null)
            {
                return NotFound();
            }
            return Ok(toReturn);
        }

        [HttpGet]
        [Route("byUser/{id}")]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        [Produces("application/json", Type = typeof(IEnumerable<PetDTO>))]
        public IActionResult GetByUserId(int id)
        {
            if (_session.UserRole != "ADMIN" && id != _session.UserId)
            {
                return Unauthorized();
            }
            return Ok(_uow.Get<IPetRepository>().GetAllByUserId(id));
        }

        [HttpGet]
        [Route("byAnimal/{name}")]
        [Produces("application/json", Type = typeof(IEnumerable<PetDTO>))]
        public IActionResult GetByAnimal(string name)
        {
            return Ok(_uow.Get<IPetRepository>().GetAllFreeByAnimal(name));
        }

        [HttpPost]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = typeof(PetInsertDTO))]
        public IActionResult Post([FromBody]PetInsertDTO dto)
        {
            PetInsertDTO newPet = _uow.Get<IPetRepository>().Insert(dto);
            _uow.Save();
            return Ok(newPet);
        }

        [HttpPut]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = null)]
        public IActionResult Put([FromBody]PetInsertDTO dto)
        {
            if (_uow.Get<IPetRepository>().Update(dto))
            {
                _uow.Save();
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete]
        [Route("{id}")]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = null)]
        public IActionResult Delete(int id)
        {
            if (_uow.Get<IPetRepository>().Delete(id))
            {
                _uow.Save();
                return Ok();
            }
            return BadRequest();
        }

        [HttpPatch]
        [Route("book/{petId}")]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        [Produces("application/json", Type = null)]
        public IActionResult Book(int petId)
        {
            PetUpdateStatusDTO old = _uow.Get<IPetRepository>().Get(petId).MapTo<PetUpdateStatusDTO>();
            if(old.UserId != null && _session.UserRole == "ADMIN")
            {
                old.PetStatusName = "BOOKED";
            }
            else
            {
                if (old.UserId != null)
                {
                    return BadRequest();
                }
                old.PetStatusName = "BOOKED";
                old.UserId = _session.UserId;
            }
            if (_uow.Get<IPetRepository>().UpdateStatus(old))
            {
                _uow.Save();
                return Ok();
            }
            return BadRequest();
        }

        [HttpPatch]
        [Route("cancel/{petId}")]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        [Produces("application/json", Type = null)]
        public IActionResult Cancel(int petId)
        {
            PetUpdateStatusDTO old = _uow.Get<IPetRepository>().Get(petId).MapTo<PetUpdateStatusDTO>();
            if (_session.UserRole != "ADMIN" && old.UserId != _session.UserId)
            {
                return Unauthorized();
            }
            if (old.PetStatusName == "SOLD" && _session.UserRole != "ADMIN")
            {
                return Unauthorized();
            }
            old.PetStatusName = null;
            old.UserId = null;
            if (_uow.Get<IPetRepository>().UpdateStatus(old))
            {
                _uow.Save();
                return Ok();
            }
            return BadRequest();
        }

        [HttpPatch]
        [Route("accept/{petId}")]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = null)]
        public IActionResult Accept(int petId)
        {
            PetUpdateStatusDTO old = _uow.Get<IPetRepository>().Get(petId).MapTo<PetUpdateStatusDTO>();
            if(old.UserId == null)
            {
                return BadRequest();
            }
            old.PetStatusName = "SOLD";
            if (_uow.Get<IPetRepository>().UpdateStatus(old))
            {
                _uow.Save();
                return Ok();
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("Image/{id}")]
        [Produces("image/png")]
        public IActionResult GetImage(int id)
        {
            PetDetailsDTO toReturn = _uow.Get<IPetRepository>().Get(id);
            if (toReturn == null || toReturn.Image == null)
            {
                return NotFound();
            }
            return File(toReturn.Image, toReturn.ImageMimeType);
        }
    }
}
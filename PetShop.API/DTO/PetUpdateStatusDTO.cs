﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.DTO
{
    public class PetUpdateStatusDTO
    {
        public int Id { get; set; }

        public string PetStatusName { get; set; }

        public int? UserId { get; set; }
    }
}

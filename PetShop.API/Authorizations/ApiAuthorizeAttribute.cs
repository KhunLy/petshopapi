﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using PetShop.API.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ToolBox.Token;

namespace PetShop.API.Authorizations
{
    public class ApiAuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public string[] Roles { get; set; }

        public ApiAuthorizeAttribute(params string[] roles)
        {
            Roles = roles;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            context.HttpContext.Request.Headers.TryGetValue("Authorization", out StringValues authHeaders);
            string token = authHeaders.FirstOrDefault(h => h.StartsWith("Bearer "))?.Replace("Bearer ", string.Empty);
            if(token == null)
            {
                context.Result = new UnauthorizedResult();
            }
            else
            {
                ITokenService tokenService = (ITokenService)context.HttpContext.RequestServices.GetService(typeof(ITokenService));
                ClaimsPrincipal claims = tokenService.ValidateToken(token);
                if(claims == null)
                {
                    context.Result = new UnauthorizedResult();
                }
                else
                {
                    string userRole = claims.FindFirstValue("RoleName");
                    if (!Roles.Contains(userRole))
                    {
                        context.Result = new UnauthorizedResult();
                    }
                    else
                    {
                        SessionService session = (SessionService)context.HttpContext.RequestServices.GetService(typeof(SessionService));
                        session.UserRole = userRole;
                        session.UserId = int.Parse(claims.FindFirstValue("Id"));
                    }
                }
            }
        }
    }
}

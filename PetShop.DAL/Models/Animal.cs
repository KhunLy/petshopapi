﻿using System;
using System.Collections.Generic;

namespace PetShop.DAL.Models
{
    public partial class Animal
    {
        public Animal()
        {
            Breed = new HashSet<Breed>();
        }

        public string Name { get; set; }

        public virtual ICollection<Breed> Breed { get; set; }
    }
}

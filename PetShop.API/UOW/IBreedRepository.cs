﻿using PetShop.API.DTO;
using System.Collections.Generic;

namespace PetShop.API.UOW
{
    public interface IBreedRepository
    {
        public IEnumerable<BreedDTO> GetAllByAnimal(string animalName);

        public IEnumerable<BreedDTO> GetAll();

        public BreedDTO Insert(BreedDTO dto);

        public bool Delete(int id);
    }
}
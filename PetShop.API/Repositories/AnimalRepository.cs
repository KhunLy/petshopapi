﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PetShop.API.DTO;
using PetShop.API.UOW;
using PetShop.DAL.Models;
using ToolBox.Mappers;

namespace PetShop.API.Repositories
{
    public class AnimalRepository : BaseRepository, IAnimalRepository
    {
        public AnimalRepository(PetShopContext context) : base(context)
        {
        }

        public IEnumerable<AnimalDTO> GetAll()
        {
            return _context.Animal.ToList().Select(a => a.MapTo<AnimalDTO>());
        }

        public AnimalDTO Insert(AnimalDTO dto)
        {
            _context.Animal.Add(dto.MapTo<Animal>());
            return dto;
        }
        public bool Delete(string name)
        {
            Animal toDelete = _context.Animal.FirstOrDefault(a => a.Name == name);
            if (toDelete == null)
            {
                return false;
            }
            _context.Animal.Remove(toDelete);
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.Session
{
    public class SessionService
    {
        public int UserId { get; set; }
        public string UserRole { get; set; }
    }
}

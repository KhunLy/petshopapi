﻿using PetShop.API.DTO;
using System.Collections.Generic;

namespace PetShop.API.UOW
{
    public interface IPetRepository
    {
        public PetDetailsDTO Get(int id);
        public IEnumerable<PetDTO> GetAll();
        public IEnumerable<PetDTO> GetAllByUserId(int id);
        public IEnumerable<PetDTO> GetAllFreeByAnimal(string animalName);
        public PetInsertDTO Insert(PetInsertDTO dto);
        public bool Update(PetInsertDTO dto);
        public bool Delete(int id);
        public bool UpdateStatus(PetUpdateStatusDTO dto);
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using PetShop.API.Authorizations;
using PetShop.API.DTO;
using PetShop.API.Session;
using PetShop.API.UOW;
using System;
using ToolBox.Token;

namespace PetShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : MasterController
    {
        public SecurityController([FromServices] UnitOfWork uow, [FromServices] SessionService session) : base(uow, session)
        {
        }

        [HttpPost]
        [Route("login")]
        [Produces("application/json", Type = typeof(string))]
        public IActionResult Login([FromBody]LoginDTO dto, [FromServices]ITokenService tokenService)
        {
            UserDTO user = _uow.Get<IUserRepository>().GetByCredentials(dto);
            if (user != null)
                return Ok(tokenService.GenerateToken(user, DateTime.Now.AddDays(1)));
            return BadRequest(new ErrorDTO("Form", "Bad Credentials"));
        }

        [HttpGet]
        [Route("refreshToken")]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        [Produces("application/json", Type = typeof(string))]
        public IActionResult Login([FromServices]ITokenService tokenService)
        {
            UserDTO user = _uow.Get<IUserRepository>().Get(_session.UserId);
            if (user != null)
                return Ok(tokenService.GenerateToken(user));
            return Unauthorized();
        }
    }
}
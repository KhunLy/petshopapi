﻿using Newtonsoft.Json;
using PetShop.API.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.DTO
{
    public class PetInsertDTO
    {
        public int Id { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [Required]
        public string Reference { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [Required]
        [NotBeforeToday]
        public DateTime BirthDate { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [Required]
        public int BreedId { get; set; }


        public byte[] Image { get; set; }
        public string ImageMimeType { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [Required]
        public bool Vaccinated { get; set; }
        public string Description { get; set; }
        public DateTime? UpdateDate { get; set; }


        public string PetStatusName { get; set; }
        public int? UserId { get; set; }
        public string BreedName { get; set; }
        public string AnimalName { get; set; }
    }
}

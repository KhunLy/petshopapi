﻿CREATE PROCEDURE [dbo].[SP_User_INSERT]
	@email NVARCHAR(255),
	@password NVARCHAR(255),
	@lastName NVARCHAR(50),
	@firstName NVARCHAR(50),
	@birthDate DATETIME2,
	@roleName VARCHAR(50)
AS
	DECLARE @salt UNIQUEIDENTIFIER;
	SET @salt = NEWID();
	INSERT INTO [User] OUTPUT INSERTED.* VALUES(
		@email,
		dbo.UDF_Hash(@password, @salt),
		@salt,
		@lastName,
		@firstName,
		@birthDate,
		@roleName
	);

RETURN 0

﻿using PetShop.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.Repositories
{
    public abstract class BaseRepository
    {
        protected readonly PetShopContext _context;

        protected BaseRepository(PetShopContext context)
        {
            _context = context;
        }
    }
}

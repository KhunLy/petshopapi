﻿CREATE TRIGGER [TR_Insert_Animal]
ON [dbo].[Animal]
AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON
	INSERT INTO Breed([Name], AnimalName) 
		SELECT 'Unknown', [Name] FROM inserted 
END

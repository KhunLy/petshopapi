﻿/*
Modèle de script de post-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront ajoutées au script de compilation.		
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de post-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de post-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

INSERT INTO PetStatus VALUES('BOOKED'), ('SOLD');
INSERT INTO [Role] VALUES('CUSTOMER'), ('ADMIN');
EXEC SP_User_Insert 'guillaume.oger@bstorm.be', 'test1234=', 'Oger', 'Guillaume', '1992-09-17', 'ADMIN';
EXEC SP_User_Insert 'a@a', 'a', 'a', 'a', '1992-09-17', 'ADMIN';
EXEC SP_User_Insert 'john@doe.be', 'test1234=', 'Doe', 'John', null, 'CUSTOMER'

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.DTO
{
    public class ErrorDTO
    {
        public Dictionary<string, List<string>> errors { get; set; }
        public ErrorDTO(string name, params string[] errors)
        {
            this.errors = new Dictionary<string, List<string>>
            {
                { name, errors.ToList() }
            };
        }
    }
}

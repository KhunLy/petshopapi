﻿namespace ToolBox.Token
{
    public class TokenServiceConfig
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string Signature { get; set; }
    }
}
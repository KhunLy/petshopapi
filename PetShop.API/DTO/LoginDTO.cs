﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.DTO
{
    public class LoginDTO
    {
        [JsonProperty(Required = Required.DisallowNull)]
        [Required]
        [EmailAddress]
        [MaxLength(255)]
        public string Email { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [Required]
        [MaxLength(255)]
        public string Password { get; set; }
    }
}

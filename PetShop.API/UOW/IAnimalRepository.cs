﻿using System.Collections.Generic;
using PetShop.API.DTO;

namespace PetShop.API.UOW
{
    public interface IAnimalRepository
    {
        bool Delete(string name);
        IEnumerable<AnimalDTO> GetAll();
        AnimalDTO Insert(AnimalDTO dto);
    }
}
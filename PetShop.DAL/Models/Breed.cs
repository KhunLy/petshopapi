﻿using System;
using System.Collections.Generic;

namespace PetShop.DAL.Models
{
    public partial class Breed
    {
        public Breed()
        {
            Pet = new HashSet<Pet>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string AnimalName { get; set; }

        public virtual Animal AnimalNameNavigation { get; set; }
        public virtual ICollection<Pet> Pet { get; set; }
    }
}

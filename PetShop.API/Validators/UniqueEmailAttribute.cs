﻿using Microsoft.AspNetCore.Mvc;
using PetShop.API.Repositories;
using PetShop.API.UOW;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.Validators
{
    public class UniqueEmailAttribute: ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            UnitOfWork uow = (UnitOfWork)context.GetService(typeof(UnitOfWork));
            if( uow.Get<IUserRepository>().GetByEmail(value.ToString()) == null)
            {
                return null;
            }
            return new ValidationResult("This Email Already Exists");
        }
    }
}

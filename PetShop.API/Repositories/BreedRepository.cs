﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PetShop.API.DTO;
using PetShop.API.UOW;
using PetShop.DAL.Models;
using ToolBox.Mappers;

namespace PetShop.API.Repositories
{
    public class BreedRepository : BaseRepository, IBreedRepository
    {
        public BreedRepository(PetShopContext context) : base(context)
        {
        }

        public bool Delete(int id)
        {
            Breed toDelete = _context.Breed.FirstOrDefault(a => a.Id == id);
            if (toDelete == null)
            {
                return false;
            }
            _context.Breed.Remove(toDelete);
            return true;
        }

        public IEnumerable<BreedDTO> GetAll()
        {
            return _context.Breed
                .ToList().Select(b => b.MapTo<BreedDTO>());
        }

        public IEnumerable<BreedDTO> GetAllByAnimal(string animalName)
        {
            return _context.Breed.Where(b => b.AnimalName == animalName)
                .ToList().Select(b => b.MapTo<BreedDTO>());
        }

        public BreedDTO Insert(BreedDTO dto)
        {
            var toInsert = dto.MapTo<Breed>();
            toInsert.Id = 0;
            _context.Breed.Add(toInsert);

            return dto;
        }
    }
}

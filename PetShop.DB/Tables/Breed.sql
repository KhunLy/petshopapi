﻿CREATE TABLE [dbo].[Breed]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Name] NVARCHAR(100) NOT NULL,
	[AnimalName] NVARCHAR(50) NOT NULL REFERENCES Animal
		ON DELETE CASCADE,
	UNIQUE([Name], AnimalName)
)

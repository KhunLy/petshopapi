﻿using System;
using System.Collections.Generic;

namespace PetShop.DAL.Models
{
    public partial class PetStatus
    {
        public PetStatus()
        {
            Pet = new HashSet<Pet>();
        }

        public string Name { get; set; }

        public virtual ICollection<Pet> Pet { get; set; }
    }
}

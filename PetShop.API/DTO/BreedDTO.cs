﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.DTO
{
    public class BreedDTO
    {
        public int Id { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [MaxLength(50)]
        [Required]
        public string AnimalName { get; set; }
    }
}

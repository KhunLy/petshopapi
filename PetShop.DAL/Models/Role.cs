﻿using System;
using System.Collections.Generic;

namespace PetShop.DAL.Models
{
    public partial class Role
    {
        public Role()
        {
            User = new HashSet<User>();
        }

        public string Name { get; set; }

        public virtual ICollection<User> User { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace PetShop.DAL.Models
{
    public partial class User
    {
        public User()
        {
            Pet = new HashSet<Pet>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public byte[] Password { get; set; }
        public Guid Salt { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string RoleName { get; set; }

        public virtual Role RoleNameNavigation { get; set; }
        public virtual ICollection<Pet> Pet { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetShop.API.DTO;
using PetShop.API.UOW;
using PetShop.DAL.Models;
using ToolBox.Mappers;

namespace PetShop.API.Repositories
{
    public class PetRepository : BaseRepository, IPetRepository
    {
        public PetRepository(PetShopContext context) : base(context)
        {
        }

        public bool Delete(int id)
        {
            Pet toDelete = _context.Pet
                .SingleOrDefault(u => u.Id == id);
            if (toDelete == null)
            {
                return false;
            }
            _context.Pet.Remove(toDelete);
            return true;
        }

        public PetDetailsDTO Get(int id)
        {
            return _context.Pet.ToList()
                .Where(p => p.Id == id)
                .Select(p => {
                    PetDetailsDTO result = p.MapTo<PetDetailsDTO>();
                    result.BreedName = p.Breed?.Name;
                    result.AnimalName = p.Breed?.AnimalName;
                    result.ImageUrl = p.Image != null ? "/Pet/Image/" + p.Id : null;
                    return result;
                }).FirstOrDefault();
        }

        public IEnumerable<PetDTO> GetAllFreeByAnimal(string animalName)
        {
            return _context.Pet.ToList()
                .Where(p => p.Breed?.AnimalName == animalName && p.UserId == null)
                .Select(p => {
                    PetDTO result = p.MapTo<PetDTO>();
                    result.BreedName = p.Breed?.Name;
                    result.AnimalName = p.Breed?.AnimalName;
                    result.ImageUrl = p.Image != null ? "/Pet/Image/" + p.Id : null;
                    return result;
                });
        }

        public IEnumerable<PetDTO> GetAllByUserId(int id)
        {
            return _context.Pet.ToList()
                .Where(p => p.UserId == id)
                .Select(p => {
                    PetDTO result = p.MapTo<PetDTO>();
                    result.BreedName = p.Breed?.Name;
                    result.AnimalName = p.Breed?.AnimalName;
                    result.ImageUrl = p.Image != null ? "/Pet/Image/" + p.Id : null;
                    return result;
                });
        }

        public PetInsertDTO Insert(PetInsertDTO dto)
        {
            Breed breed = _context.Breed.FirstOrDefault(b => b.Id == dto.BreedId);
            Pet toInsert = dto.MapTo<Pet>();
            toInsert.Breed = breed;
            _context.Pet.Add(toInsert);
            return dto;
        }

        public bool Update(PetInsertDTO dto)
        {
            Pet toUpdate = _context.Pet.FirstOrDefault(p => p.Id == dto.Id);
            if (toUpdate == null) return false;
            Breed breed = _context.Breed.FirstOrDefault(b => b.Id == dto.BreedId);
            toUpdate.Breed = breed;
            dto.MapToInstance(toUpdate);
            return true;
        }

        public bool UpdateStatus(PetUpdateStatusDTO dto)
        {
            Pet toUpdate = _context.Pet.FirstOrDefault(p => p.Id == dto.Id);
            if (toUpdate == null) return false;
            dto.MapToInstance(toUpdate);
            return true;
        }

        public IEnumerable<PetDTO> GetAll()
        {
            return _context.Pet.ToList()
                .Select(p => {
                    PetDTO result = p.MapTo<PetDTO>();
                    result.BreedName = p.Breed?.Name;
                    result.AnimalName = p.Breed?.AnimalName;
                    result.ImageUrl = p.Image != null ? "/Pet/Image/" + p.Id : null;
                    return result;
                });
        }
    }
}

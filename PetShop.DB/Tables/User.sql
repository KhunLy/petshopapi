﻿CREATE TABLE [dbo].[User]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Email] NVARCHAR(255) NOT NULL UNIQUE,
	[Password] VARBINARY(MAX) NOT NULL,
	[Salt] UNIQUEIDENTIFIER NOT NULL UNIQUE,
	[LastName] NVARCHAR(50) NOT NULL,
	[FirstName] NVARCHAR(50) NOT NULL,
	[BirthDate] DATETIME2 NULL,
	[RoleName] VARCHAR(50) NULL REFERENCES [Role] ON DELETE SET NULL
)
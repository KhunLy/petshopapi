﻿using Newtonsoft.Json;
using PetShop.API.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.DTO
{
    public class RegisterDTO
    {
        [JsonProperty(Required = Required.DisallowNull)]
        [Required]
        [EmailAddress]
        [UniqueEmail]
        [MaxLength(255)]
        public string Email { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [Required]
        [MaxLength(255)]
        public string Password { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [NotBeforeToday]
        public DateTime? BirthDate { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using PetShop.API.Session;
using PetShop.API.UOW;

namespace PetShop.API.Controllers
{
    public abstract class MasterController : ControllerBase
    {
        protected readonly UnitOfWork _uow;
        protected readonly SessionService _session;
        public MasterController([FromServices]UnitOfWork uow, [FromServices]SessionService session)
        {
            _uow = uow;
            _session = session;
        }
    }
}
﻿using System;
using System.Security.Claims;

namespace ToolBox.Token
{
    public interface ITokenService
    {
        string GenerateToken<T>(T payload);
        string GenerateToken<T>(T payload, DateTime exp);
        ClaimsPrincipal ValidateToken(string authToken);
    }
}
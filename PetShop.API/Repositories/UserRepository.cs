﻿using PetShop.API.DTO;
using PetShop.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ToolBox.Mappers;
using PetShop.API.UOW;

namespace PetShop.API.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(PetShopContext context) : base(context)
        {
        }

        public bool Delete(int key)
        {
            User toDelete = _context.User
                .SingleOrDefault(u => u.Id == key);
            if(toDelete == null)
            {
                return false;
            }
            _context.User.Remove(toDelete);
            return true;
        }

        public UserDTO Get(int key)
        {
            return _context.User
                .SingleOrDefault(u => u.Id == key)?.MapTo<UserDTO>();
        }

        public UserDTO GetByCredentials(LoginDTO login)
        {
            return _context.User.FromSqlRaw(
                "dbo.SP_User_Select_By_Credentials @p0, @p1", 
                login.Email, 
                login.Password
            ).ToList().FirstOrDefault()?.MapTo<UserDTO>();
        }

        public IEnumerable<UserDTO> GetAll()
        {
            return _context.User.ToList().Select(u => u.MapTo<UserDTO>());
        }

        public UserDTO Insert(RegisterDTO entity)
        {
            return _context.User.FromSqlRaw(
                "dbo.SP_User_Insert @p0, @p1, @p2, @p3, @p4, @p5",
                entity.Email,
                entity.Password,
                entity.LastName,
                entity.FirstName,
                (object)entity.BirthDate ?? DBNull.Value,
                "CUSTOMER"
            ).ToList().FirstOrDefault()?.MapTo<UserDTO>();
        }

        public bool Update(UserUpdateDTO entity)
        {
            User toUpdate = _context.User.FirstOrDefault(u => u.Id == entity.Id);
            if (toUpdate == null) return false;
            entity.MapToInstance(toUpdate);
            return true;
        }

        public UserDTO GetByEmail(string email)
        {
            return _context.User
                .SingleOrDefault(u => u.Email.ToLower() == email.ToLower())?.MapTo<UserDTO>();
        }
    }
}

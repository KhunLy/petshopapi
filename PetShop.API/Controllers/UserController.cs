﻿using Microsoft.AspNetCore.Mvc;
using PetShop.API.Authorizations;
using PetShop.API.DTO;
using PetShop.API.Session;
using PetShop.API.UOW;
using System.Collections.Generic;

namespace PetShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : MasterController
    {
        public UserController([FromServices] UnitOfWork uow, [FromServices] SessionService session) : base(uow, session)
        {
        }

        [HttpPost]
        [Produces("application/json", Type = typeof(RegisterDTO))]
        public IActionResult Post([FromBody]RegisterDTO user)
        {
            UserDTO dto = _uow.Get<IUserRepository>().Insert(user);
            _uow.Save();
            return Ok(dto);
        }

        [HttpGet]
        [Route("{id}")]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        [Produces("application/json", Type = typeof(UserDTO))]
        public IActionResult Get(int id)
        {
            if (_session.UserRole != "ADMIN" && id != _session.UserId)
            {
                return Unauthorized();
            }
            UserDTO dto = _uow.Get<IUserRepository>().Get(id);
            if (dto == null) return NotFound();
            return Ok(dto);
        }

        [HttpGet]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = typeof(IEnumerable<UserDTO>))]
        public IActionResult Get()
        {
            return Ok(_uow.Get<IUserRepository>().GetAll());
        }

        [HttpDelete]
        [Route("{id}")]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        [Produces("application/json")]
        public IActionResult Delete(int id)
        {
            if (_session.UserRole != "ADMIN" && id != _session.UserId)
            {
                return Unauthorized();
            }
            if (_uow.Get<IUserRepository>().Delete(id))
            {
                _uow.Save();
                return Ok();
            }
            return BadRequest();
        }

        [HttpPut]
        [ApiAuthorize("ADMIN", "CUSTOMER")]
        [Produces("application/json")]
        public IActionResult Put([FromBody]UserUpdateDTO user)
        {
            if (_session.UserRole != "ADMIN" && user.Id != _session.UserId)
            {
                return Unauthorized();
            }
            if (_uow.Get<IUserRepository>().Update(user))
            {
                _uow.Save();
                return Ok();
            }
            return BadRequest();
        }
    }
}
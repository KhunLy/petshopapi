﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PetShop.API.Authorizations;
using PetShop.API.DTO;
using PetShop.API.Session;
using PetShop.API.UOW;

namespace PetShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BreedController : MasterController
    {
        public BreedController([FromServices] UnitOfWork uow, [FromServices] SessionService session) : base(uow, session)
        {
        }

        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<BreedDTO>))]
        public IActionResult Get()
        {
            return Ok(_uow.Get<IBreedRepository>().GetAll());
        }

        [HttpGet]
        [Route("byAnimal/{animal}")]
        [Produces("application/json", Type = typeof(IEnumerable<BreedDTO>))]
        public IActionResult GetByAnimal(string animal)
        {
            return Ok(_uow.Get<IBreedRepository>().GetAllByAnimal(animal));
        }

        [HttpPost]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = typeof(BreedDTO))]
        public IActionResult Post([FromBody]BreedDTO dto)
        {
            IAnimalRepository aRepo = _uow.Get<IAnimalRepository>();
            if(aRepo.GetAll().FirstOrDefault(a => a.Name == dto.AnimalName) == null)
            {
                return BadRequest(new ErrorDTO("Name", "Verifie tes données"));
            }
            IEnumerable<BreedDTO> breeds = _uow.Get<IBreedRepository>().GetAll();
            if(breeds.FirstOrDefault(b => b.Name == dto.Name && b.AnimalName.ToLower() == dto.AnimalName.ToLower()) != null)
            {
                return BadRequest(new ErrorDTO("Name", "This name already exists"));
            }
            _uow.Get<IBreedRepository>().Insert(dto);
            _uow.Save();
            dto.Id = _uow.Get<IBreedRepository>()
                .GetAll().FirstOrDefault(b => b.Name == dto.Name && b.AnimalName == dto.AnimalName)?.Id ?? 0;
            return Ok(dto);
        }

        [HttpDelete]
        [Route("{id}")]
        [ApiAuthorize("ADMIN")]
        [Produces("application/json", Type = null)]
        public IActionResult Delete(int id)
        {
            if (_uow.Get<IBreedRepository>().Delete(id))
            {
                _uow.Save();
                return Ok();
            }
            return BadRequest();
        }
    }
}
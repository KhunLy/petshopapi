﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PetShop.DAL.Models
{
    public partial class PetShopContext : DbContext
    {
        public PetShopContext()
        {
        }

        public PetShopContext(DbContextOptions<PetShopContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Animal> Animal { get; set; }
        public virtual DbSet<Breed> Breed { get; set; }
        public virtual DbSet<Pet> Pet { get; set; }
        public virtual DbSet<PetStatus> PetStatus { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Animal>(entity =>
            {
                entity.HasKey(e => e.Name)
                    .HasName("PK__Animal__737584F793C8C6A6");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Breed>(entity =>
            {
                entity.HasIndex(e => new { e.Name, e.AnimalName })
                    .HasName("UQ__Breed__1491C7802D2F5548")
                    .IsUnique();

                entity.Property(e => e.AnimalName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.AnimalNameNavigation)
                    .WithMany(p => p.Breed)
                    .HasForeignKey(d => d.AnimalName)
                    .HasConstraintName("FK__Breed__AnimalNam__45F365D3");
            });

            modelBuilder.Entity<Pet>(entity =>
            {
                entity.HasIndex(e => e.Reference)
                    .HasName("UQ__Pet__062B9EB8842D6052")
                    .IsUnique();

                entity.Property(e => e.ImageMimeType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PetStatusName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Reference)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.UpdateDate).HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Breed)
                    .WithMany(p => p.Pet)
                    .HasForeignKey(d => d.BreedId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK__Pet__BreedId__46E78A0C");

                entity.HasOne(d => d.PetStatusNameNavigation)
                    .WithMany(p => p.Pet)
                    .HasForeignKey(d => d.PetStatusName)
                    .HasConstraintName("FK__Pet__PetStatusNa__47DBAE45");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Pet)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK__Pet__UserId__48CFD27E");
            });

            modelBuilder.Entity<PetStatus>(entity =>
            {
                entity.HasKey(e => e.Name)
                    .HasName("PK__PetStatu__737584F72F835121");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.Name)
                    .HasName("PK__Role__737584F7C6F03E76");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.Email)
                    .HasName("UQ__User__A9D10534693453D1")
                    .IsUnique();

                entity.HasIndex(e => e.Salt)
                    .HasName("UQ__User__A152BCCE8B06D9F6")
                    .IsUnique();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Password).IsRequired();

                entity.Property(e => e.RoleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.RoleNameNavigation)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.RoleName)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK__User__RoleName__49C3F6B7");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

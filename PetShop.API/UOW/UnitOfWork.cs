﻿using Microsoft.AspNetCore.Mvc;
using PetShop.DAL.Models;
using System;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetShop.API.Repositories;

namespace PetShop.API.UOW
{
    public class UnitOfWork : IDisposable
    {
        private readonly PetShopContext _context;

        private readonly ServiceProvider _provider;

        public UnitOfWork([FromServices]PetShopContext context)
        {
            _context = context;
            ServiceCollection collection = new ServiceCollection();
            collection.AddTransient(provider => context);
            collection.AddTransient<IUserRepository,UserRepository>();
            collection.AddTransient<IAnimalRepository,AnimalRepository>();
            collection.AddTransient<IPetRepository,PetRepository>();
            collection.AddTransient<IBreedRepository,BreedRepository>();
            _provider = collection.BuildServiceProvider();
        }

        public TRepository Get<TRepository>()
        {
            return _provider.GetService<TRepository>();
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _context.Dispose();
                _provider.Dispose();
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
        }
    }
}

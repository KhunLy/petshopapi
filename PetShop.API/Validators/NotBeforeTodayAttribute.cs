﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop.API.Validators
{
    public class NotBeforeTodayAttribute: ValidationAttribute
    {
        public NotBeforeTodayAttribute()
        {
            ErrorMessage = "This Date Should be Before Today";
        }

        public override bool IsValid(object value)
        {
            if (value == null) return true;
            return (DateTime)value < DateTime.Now;
        }
    }
}
